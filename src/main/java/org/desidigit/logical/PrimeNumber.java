package org.desidigit.logical;

import java.util.Scanner;

public class PrimeNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Inter Number:-");
        int number = scanner.nextInt();
        boolean result = checkPrimeNumber(number);
        System.out.println("Is prime number: " + result);
    }

    public static boolean checkPrimeNumber(int input) {
        boolean flage = true;
        for (int i = 2; i <= input / 2; i++) {
            if (input % i == 0) {
                flage = false;
                break;
            }
        }
        return flage;
    }
}
