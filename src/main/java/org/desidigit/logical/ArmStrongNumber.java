package org.desidigit.logical;

public class ArmStrongNumber {
    public static void main(String[] arg) {
        int input = 153;
        int number = input;
        int remainder;
        int sum = 0;
        while (input > 0) {
            remainder = input % 10;
            input = input / 10;
            sum = sum + remainder * remainder * remainder;
        }
        if (number == sum) {
            System.out.println("it is armstrong number");
        } else {
            System.out.println("it is not armstrong number");
        }
    }
}