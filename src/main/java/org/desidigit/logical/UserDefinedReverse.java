package org.desidigit.logical;

public class UserDefinedReverse {
    public static void main(String[] arg) {
        StringBuilder stringBuilder = new StringBuilder("hello");
        String str = "Hello World";
        String reversedString = reverse(str);
        System.out.println(reversedString);
    }

    public static String reverse(String str) {
        StringBuilder stringBuilder = new StringBuilder();
        int len = str.length();
        for (int i = len - 1; i >= 0; i--) {
            stringBuilder.append(str.charAt(i));
        }
        return stringBuilder.toString();
    }
}
