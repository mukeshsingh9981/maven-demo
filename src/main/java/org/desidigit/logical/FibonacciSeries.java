package org.desidigit.logical;

public class FibonacciSeries {
    public static void main(String[] args) {
        int firstTerm = 0;
        int secondTerm = 1;
        int sum;
        System.out.print(secondTerm);
        for (int i = 1; i < 10; i++) {
            sum = firstTerm + secondTerm;
            System.out.print(" " + sum);
            firstTerm = secondTerm;
            secondTerm = sum;
        }
    }
}