package org.desidigit.logical;

public class PolimdromString {
    public static void main(String[] args) {
        String input = "madam";
        boolean result = checkPolimdrom(input);

        System.out.println("Is " + input + " palindrome: " + result);
    }

    public static boolean checkPolimdrom(String input) {
        int len = input.length();
        for (int i = 0; i < len/2; i++) {
            if (input.charAt(i) != input.charAt(len - 1 - i)) {
                return false;
            }
        }
        return true;
    }
}
