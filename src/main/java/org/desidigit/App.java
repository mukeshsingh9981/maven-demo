package org.desidigit;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        Student student = new Student(1, "ABC");
        System.out.println(student);
    }
}
